.PHONY: build test clean

build: libmax.so

libmax.so: max.o
	gcc -o $@  -shared $<

max.o: max.c
	gcc -c -fPIC $<

test: test.c libmax.so
	gcc test.c -L. -lmax -o test
	
clean:
	rm -f *.o *.so a.out